package com.epam.rd.java.basic.task8;


public class Flower {
    String name;
    String soil;
    String origin;
    String stemColour;
    String leafColour;
    int aveLenFlower;
    int tempreture;
    String lighting;
    int watering;
    String multiplying;

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public int getTempreture() {
        return tempreture;
    }

    public String getLighting() {
        return lighting;
    }

    public int getWatering() {
        return watering;
    }

    public String getMultiplying() {
        return multiplying;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public Flower(String name, String soil, String origin, String stemColour,
                  String leafColour, int aveLenFlower, int tempreture, String lighting,
                  int watering, String multiplying) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
        this.tempreture = tempreture;
        this.lighting = lighting;
        this.watering = watering;
        this.multiplying = multiplying;
    }

    public Flower (String name){
        this.name = name;
    }

    public Flower() {

    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                ", tempreture=" + tempreture +
                ", lighting='" + lighting + '\'' +
                ", watering=" + watering +
                ", multiplying='" + multiplying + '\'' +
                "}\n";
    }
}
