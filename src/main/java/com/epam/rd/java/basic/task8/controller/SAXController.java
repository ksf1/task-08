package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

	private String xmlFileName;
	private static ArrayList<Flower> flowerList = new ArrayList<Flower>();

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void parseXML() throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		XMLHandler handler = new XMLHandler();
		parser.parse(xmlFileName, handler);

	}

	private static class XMLHandler extends DefaultHandler{
		private String name, soil, origin, stemColour, leafColour, lighting, multiplying, lastElementName;
		private String aveLenFlower, tempreture, watering;
		@Override
		public void startElement(String uri, String localName,
								 String qName, Attributes attributes){
			lastElementName = qName;
			if (qName.equals("lighting")){
				lighting = attributes.getValue("lightRequiring");
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			String information = new String(ch, start, length);

			information = information.replace("\n", "").trim();

			if (!information.isEmpty()) {
				if (lastElementName.equals("name")){
					name = information;
				}
				if (lastElementName.equals("soil")){
					soil = information;
				}
				if (lastElementName.equals("origin")){
					origin = information;
				}
				if (lastElementName.equals("stemColour")){
					stemColour = information;
				}
				if (lastElementName.equals("leafColour")){
					leafColour = information;
				}
				if (lastElementName.equals("lighting")){
					lighting = information;
				}
				if(lastElementName.equals("multiplying")){
					multiplying = information;
				}
				if(lastElementName.equals("aveLenFlower")){
					aveLenFlower = information;
				}
				if(lastElementName.equals("tempreture")){
					tempreture = information;
				}
				if(lastElementName.equals("watering")){
					watering = information;
				}

			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if ( (name != null && !name.isEmpty())
					&& (soil != null && !soil.isEmpty())
					&& (origin != null && !origin.isEmpty())
					&& (stemColour != null && !stemColour.isEmpty())
					&& (leafColour != null && !leafColour.isEmpty())
					&& (multiplying != null && !multiplying.isEmpty())
					&& (aveLenFlower != null && !aveLenFlower.isEmpty())
					&& (tempreture != null && !tempreture.isEmpty())
					&& (watering != null && !watering.isEmpty())
					&& (lighting != null && !lighting.isEmpty())) {
				flowerList.add(new Flower(name, soil, origin, stemColour, leafColour, Integer.parseInt(aveLenFlower),
						Integer.parseInt(tempreture), lighting, Integer.parseInt(watering), multiplying));
				name = null;
				soil = null;
				origin = null;
				stemColour = null;
				leafColour = null;
				aveLenFlower = null;
				tempreture = null;
				lighting = null;
				watering = null;
				multiplying = null;
			}
		}
	}

	public ArrayList<Flower> sort(){

		flowerList.sort((o1, o2) -> o1.getSoil().compareTo(o2.getSoil()));
		return flowerList;
	}

	public void SAXFormOutput(String FileName) throws IOException, TransformerException, ParserConfigurationException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		Document doc = docBuilder.newDocument();
		Element trueRootElement = doc.createElement("flowers");
		trueRootElement.setAttribute("xmlns","http://www.nure.ua");
		trueRootElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		trueRootElement.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd ");
		doc.appendChild(trueRootElement);


		for (Flower f:flowerList) {
			Element rootElement = doc.createElement("flower");
			trueRootElement.appendChild(rootElement);

			Element name=doc.createElement("name");
			name.setTextContent(f.getName());

			rootElement.appendChild(name);

			Element soil=doc.createElement("soil");
			soil.setTextContent(f.getSoil());
			rootElement.appendChild(soil);

			Element origin=doc.createElement("origin");
			origin.setTextContent(f.getOrigin());
			rootElement.appendChild(origin);

			Element visualParameters= doc.createElement("visualParameters");
			rootElement.appendChild(visualParameters);

			Element stemColour= doc.createElement("stemColour");
			stemColour.setTextContent(f.getStemColour());
			visualParameters.appendChild(stemColour);


			Element leafColour= doc.createElement("leafColour");
			leafColour.setTextContent(f.getLeafColour());
			visualParameters.appendChild(leafColour);


			Element aveLenFlower= doc.createElement("aveLenFlower");
			aveLenFlower.setAttribute("measure","cm");
			aveLenFlower.setTextContent(String.valueOf(f.getAveLenFlower()));
			visualParameters.appendChild(aveLenFlower);

			Element growingTips = doc.createElement("growingTips");
			rootElement.appendChild(growingTips);

			Element tempreture= doc.createElement("tempreture");
			tempreture.setAttribute("measure","celcius");
			tempreture.setTextContent(String.valueOf(f.getTempreture()));
			growingTips.appendChild(tempreture);

			Element lighting = doc.createElement("lighting");
			lighting.setAttribute("lightRequiring",f.getLighting());
			growingTips.appendChild(lighting);

			Element watering = doc.createElement("watering");
			watering.setAttribute("measure","mlPerWeek");
			watering.setTextContent(String.valueOf(f.getWatering()));
			growingTips.appendChild(watering);

			Element multiplying = doc.createElement("multiplying");
			multiplying.setTextContent(f.getMultiplying());
			rootElement.appendChild(multiplying);
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(FileName));

		transformer.transform(source, result);
	}
}