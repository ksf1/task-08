package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;
    private ArrayList<Flower> flowerList = new ArrayList<Flower>();

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void parseXML() throws FileNotFoundException, XMLStreamException {
        Flower flower = null;
        int c = 0;
        XMLInputFactory factory = XMLInputFactory.newFactory();
        try {
            XMLEventReader reader = factory.createXMLEventReader(new FileInputStream(xmlFileName));
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    if (startElement.getName().getLocalPart().equals("flower")) {
						flower = new Flower();

						} else if (startElement.getName().getLocalPart().equals("name")) {
                            event = reader.nextEvent();
                            flower.setName(event.asCharacters().getData());

                        } else if (startElement.getName().getLocalPart().equals("soil")) {
                            event = reader.nextEvent();
                            flower.setSoil(event.asCharacters().getData());
                        } else if (startElement.getName().getLocalPart().equals("origin")) {
                            event = reader.nextEvent();
                            flower.setOrigin(event.asCharacters().getData());
                        } else if (startElement.getName().getLocalPart().equals("stemColour")) {
                            event = reader.nextEvent();
                            flower.setStemColour(event.asCharacters().getData());
                        } else if (startElement.getName().getLocalPart().equals("leafColour")) {
                            event = reader.nextEvent();
                            flower.setLeafColour(event.asCharacters().getData());
                        } else if (startElement.getName().getLocalPart().equals("aveLenFlower")) {
                            event = reader.nextEvent();
                            flower.setAveLenFlower(Integer.parseInt(event.asCharacters().getData()));
                        } else if (startElement.getName().getLocalPart().equals("tempreture")) {
                            event = reader.nextEvent();
                            flower.setTempreture(Integer.parseInt(event.asCharacters().getData()));
                        } else if (startElement.getName().getLocalPart().equals("watering")) {
                            event = reader.nextEvent();
                            flower.setWatering(Integer.parseInt(event.asCharacters().getData()));
                        } else if (startElement.getName().getLocalPart().equals("lighting")) {
                    		Attribute attribute = startElement.getAttributeByName(new QName("lightRequiring"));
                            if (attribute != null){
                            	flower.setLighting(attribute.getValue());
							}
                            event = reader.nextEvent();
                        } else if (startElement.getName().getLocalPart().equals("multiplying")) {
                            event = reader.nextEvent();
                            flower.setMultiplying(event.asCharacters().getData());
                        }

                    }

                if (event.isEndElement()) {
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals("flower")) {
                        flowerList.add(flower);
                    }
                }

			}
		} catch (FileNotFoundException | XMLStreamException exception) {
            exception.printStackTrace();
        }

    }

	public ArrayList<Flower> sort(){

		flowerList.sort((o1, o2) -> o1.getOrigin().compareTo(o2.getOrigin()));
		return flowerList;
	}

	public void STAXFormOutput(String FileName) throws IOException, TransformerException, ParserConfigurationException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		Document doc = docBuilder.newDocument();
		Element trueRootElement = doc.createElement("flowers");
		trueRootElement.setAttribute("xmlns","http://www.nure.ua");
		trueRootElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		trueRootElement.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd ");
		doc.appendChild(trueRootElement);


		for (Flower f:flowerList) {
			Element rootElement = doc.createElement("flower");
			trueRootElement.appendChild(rootElement);

			Element name=doc.createElement("name");
			name.setTextContent(f.getName());

			rootElement.appendChild(name);

			Element soil=doc.createElement("soil");
			soil.setTextContent(f.getSoil());
			rootElement.appendChild(soil);

			Element origin=doc.createElement("origin");
			origin.setTextContent(f.getOrigin());
			rootElement.appendChild(origin);

			Element visualParameters= doc.createElement("visualParameters");
			rootElement.appendChild(visualParameters);

			Element stemColour= doc.createElement("stemColour");
			stemColour.setTextContent(f.getStemColour());
			visualParameters.appendChild(stemColour);


			Element leafColour= doc.createElement("leafColour");
			leafColour.setTextContent(f.getLeafColour());
			visualParameters.appendChild(leafColour);


			Element aveLenFlower= doc.createElement("aveLenFlower");
			aveLenFlower.setAttribute("measure","cm");
			aveLenFlower.setTextContent(String.valueOf(f.getAveLenFlower()));
			visualParameters.appendChild(aveLenFlower);

			Element growingTips = doc.createElement("growingTips");
			rootElement.appendChild(growingTips);

			Element tempreture= doc.createElement("tempreture");
			tempreture.setAttribute("measure","celcius");
			tempreture.setTextContent(String.valueOf(f.getTempreture()));
			growingTips.appendChild(tempreture);

			Element lighting = doc.createElement("lighting");
			lighting.setAttribute("lightRequiring",f.getLighting());
			growingTips.appendChild(lighting);

			Element watering = doc.createElement("watering");
			watering.setAttribute("measure","mlPerWeek");
			watering.setTextContent(String.valueOf(f.getWatering()));
			growingTips.appendChild(watering);

			Element multiplying = doc.createElement("multiplying");
			multiplying.setTextContent(f.getMultiplying());
			rootElement.appendChild(multiplying);
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(FileName));

		transformer.transform(source, result);
	}

}