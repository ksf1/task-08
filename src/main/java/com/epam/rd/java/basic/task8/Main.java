package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		DOMController domController = new DOMController(xmlFileName);
		domController.parseXML();
		domController.sort();

		String outputXmlFile = "output.dom.xml";
		domController.DOMFormOutput(outputXmlFile);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		saxController.parseXML();
		saxController.sort();

		outputXmlFile = "output.sax.xml";
		saxController.SAXFormOutput(outputXmlFile);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parseXML();
		staxController.sort();

		outputXmlFile = "output.stax.xml";
		staxController.STAXFormOutput(outputXmlFile);
	}

}
